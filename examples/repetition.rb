$:.unshift File.dirname(__FILE__) + "/../lib"

require 'par'
include Par

def greedy
  result = ''
  loop do
    r = yield
    break if !r || r.bottom?

    result << r
  end

  result
end
def ab_lang s
  s.string('a') >>
    greedy { s.string('b') } >>
    s.string('a')
end

%w(
  abbba
  abbbbbba
  abbb
).each do |example|
  s = source(example)
  r = ab_lang(s)

  puts "Parsing #{example.inspect} yields #{r.inspect}."
end


