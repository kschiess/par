$:.unshift File.dirname(__FILE__) + "/../lib"

require 'par'
include Par

def integer s
  s.pattern(/\+|-/).maybe >>
    s.pattern(/\d+/)
end
def float s
  s.pattern(/\+|-/).maybe >>
    s.pattern(/\d+/) >> 
    s.string('.') >> s.pattern(/\d+/)
end
def element s
  s.either { float(s) }.or { integer(s) }
end

%w(
  1.23
  232
).each do |example|
  s = source(example)
  r = element(s)

  puts "Parsing #{example.inspect} yields #{r.inspect}."
end

