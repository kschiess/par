$:.unshift File.dirname(__FILE__) + "/../lib"

require 'par'
include Par

def integer source
  source.pattern(/\+|-/).maybe >>
    source.pattern(/\d+/)
end

p integer(source('1234'))
p integer(source('+23'))
p integer(source('abc'))
