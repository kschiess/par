require 'spec_helper'

describe Par do
  include Par

  let(:s) { source 'foobar' }

  describe "sequences" do
    it "parse" do
      (s.match('foo') >> s.match('bar')).should_not == bottom
    end
    it "or not" do
      (s.match('bar') >> s.match('bar')).should == bottom
    end
  end
  describe 'maybe' do
    it "allows optional parsing" do
      (s.match('baz').maybe >> s.match('foobar')).should == 'foobar'
    end
    it "allows maybe on results" do
      s.match('foo').maybe.should == 'foo'
    end
  end
end
