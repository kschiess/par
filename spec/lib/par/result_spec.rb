require 'spec_helper'

describe Par::Result do
  include Par

  context 'a simple result' do
    let(:r) { result 'r' }

    it 'should allow equality comparison as part of an array' do
      [r, r].should == ['r', 'r']
      ['r', 'r'].should == [r, r]
    end
    it 'should not be bottom' do 
      r.should_not be_bottom
    end
    it 'should allow concatenation with any object, using #+ to form a composite' do
      o = 's'
      c = r >> o

      c.obj.should == 'rs'
    end
    it 'converts to a string (by delegating to obj)' do 
      ('a' + r).should == 'ar'
    end
    it "allows replacing contents using .as(obj)" do
      r.as(nil).should == nil
    end
    it 'allows transforming object contents' do
      r.as { |o| o + 's' }.should == 'rs'
    end
    it 'allows conversion into another value' do
      r.as('v').should == result('v')

      r.as(result('v')).obj.should eql('v')

      r.as(bottom).should eql(bottom)
      r.as(top).should eql(top)
    end
  end
  context 'given 2 results' do
    let(:a) { result 'foo' }
    let(:b) { result 'bar' }
    
    it "should allow concatenation" do
      (a >> b).to_s.should == 'foobar'
    end
    it "allow calling maybe" do
      a.maybe.should == a
    end
  end
  context '.bottom' do
    let(:any) { result 'any' }

    it "cannot be concatenated left" do
      (bottom >> any).should == bottom
    end
    it "cannot be concatenated right" do
      (any >> bottom).should == bottom 
    end
    it "can be tested for bottom?" do 
      bottom.should be_bottom
    end
    it "swallows .as(obj)" do
      bottom.as('something').should == bottom
    end 
    it "swallows .as { ... }" do
      bottom.as { fail }.should == bottom
    end 
  end
  context '.top' do
    let(:any) { result('any') }
    it 'allows concatenation left' do
      (top >> any).should == any
    end
    it 'allows concatenation right' do
      (any >> top).should == any
    end
    it 'can be maybeed' do
      top.maybe.should == top
    end
    it 'should not be bottom' do 
      top.should_not be_bottom
    end
    it 'allows turning into an object' do
      top.as('something').should == 'something'
    end
    it "allows transformation into an object" do
      top.as { |o| 'obj' }.should == 'obj'
    end 
    it "does correct value conversion" do
      top.as(bottom).should eql(bottom)
      top.as(top).should eql(top)      
    end
  end
end
