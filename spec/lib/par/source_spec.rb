# Encoding: UTF-8
require 'spec_helper'

describe Par::Source do
  include Par

  describe ".source" do
    it "constructs a source" do
      source('foo').should be_kind_of(described_class)
    end
  end
  describe "#n" do
    let(:s) { source 'äöü' }
    it "consumes n characters" do
      s.n(1).should == 'ä'
      s.n(2).should == 'öü'
    end
    it "returns empty string on end of input" do
      s.n(3).should == 'äöü'
      s.n(10).should == Par.bottom
    end
  end
  describe "#match" do
    let(:s) { source 'foobar' }

    it "matches a pattern and consumes it" do
      r = s.match /foo/
      r == 'foo'
    end
    it "returns bottom if no match" do
      s.match(/bar/).should == bottom
    end 
  end
  describe "#cr" do
    let(:s) { source 'foobar' }

    it 'rewinds the source if the block exits with Par.bot' do
      s.cr { 
        s.match 'foo'
        Par.bottom
      }

      s.rest.should == 'foobar'
    end
    it "doesn't rewind if the block has other return values" do
      s.cr {
        s.match 'foo'
      }
      s.rest.should == 'bar'
    end
  end
  describe "#at_eos?" do
    it "indicates end of string" do
      s = source('foo')
      expect(s).not_to be_at_eos

      s.n(3)
      expect(s).to be_at_eos
    end
  end
end
