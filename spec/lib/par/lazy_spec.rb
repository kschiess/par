require 'spec_helper'

describe Par::Lazy do
  include Par

  let(:source) { Par::Source.new('holler') }

  describe '#either' do
    it "creates a lazy block" do
      source.either { raise }
    end 
  end
  describe 'chaining' do
    let(:any) { result 'any' }

    it "allows a long chain of .ors" do
      source.either { bottom }.or { bottom }.or { bottom }.or { any }.value.should == any
    end
  end
  describe 'behaviour with bottom and top' do
    let(:any) { result 'any' }

    it "bottom or any" do
      source.either { bottom }.or { any }.value.should == any
    end
    it "any or bottom" do
      source.either { any }.or { bottom }.value.should == any
    end
    it "top or any" do
      source.either { top }.or { any }.value.should == top
    end
    it "any or top" do
      source.either { any }.or { top }.value.should == any
    end
  end
  describe 'rewinding' do
    it 'rewinds if the block returns bottom' do
      lazy = source.either { 
        source.n 4
        bottom
      }

      lazy.value.should == bottom
      source.pos.should == 0
    end
  end
end