
require 'par/result'
require 'par/source'
require 'par/lazy'

module Par

module_function
  def result str
    Result.new str
  end
  def source str
    Source.new str
  end
  def bottom
    Result.bottom
  end
  def top
    Result.top
  end
end
