require 'par/result/conversion'

module Par
  class Result
    include Conversion

    attr_reader :obj

    def initialize obj
      @obj = obj
    end

    def >> other
      # Concatenation with another Result
      if other.respond_to? :obj
        return concat(obj, other.obj)
      end

      # Concatenation with bottom
      return bottom if other == bottom
      return self if other == top

      # Wild concatenation with any other object
      return concat(obj, other)
    end
    def maybe
      self
    end

    def new obj
      self.class.new obj
    end
    def bottom
      self.class.bottom
    end
    def top
      self.class.top
    end

    def bottom?
      false
    end
    
    def concat obj1, obj2
      new(obj1 + obj2)
    end

    def to_str
      obj.to_str
    end
    def to_s
      obj.to_s
    end
    def inspect
      "result(#{obj.inspect})"
    end

    def <=> other
      obj <=> other
    end
    include Comparable

    def self.bottom
      @bottom ||= Bottom.new
    end
    def self.top
      @top ||= Top.new
    end
  end
end

require 'par/result/top'
require 'par/result/bottom'
