require 'strscan'

module Par
  # Source consumes a string bit by bit. It defines methods to consume a
  # string conditionally or unconditionally, never touching the original
  # string,  instead slicing it up into small results.
  #
  # String access methods are relative to the current position, unless
  # mentioned otherwise:
  #
  # * #n reads n characters
  #
  class Source
    attr_reader :str 
    attr_reader :pos

    def initialize str
      @scanner = StringScanner.new(str)
    end

    def match pattern
      pattern = Regexp.new(
        Regexp.escape(pattern)) if pattern.respond_to? :to_str

      if match = @scanner.scan(pattern) 
        return Par.result(match)
      end

      Par.bottom
    end
    
    def n n
      match = @scanner.scan(/.{#{n}}/)
      match && Par.result(match) || Par.bottom
    end

    def conditional_rewind
      old_pos = pos
      r = yield
      old_pos = pos if r != Par.bottom

      return r
    ensure
      @scanner.pos = old_pos
    end
    alias :cr :conditional_rewind

    def either &block
      Lazy.new self, &block
    end

    def rest
      @scanner.rest
    end
    def pos
      @scanner.pos
    end
    def at_eos?
      @scanner.eos?
    end
  end
end
