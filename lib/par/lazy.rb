module Par
  class Lazy
    class Elem
      def initialize source, block
        @source = source
        @block = block
      end        
      def value 
        @source.conditional_rewind {
          @block.call
        }
      end
    end

    def initialize source, &block
      @source = source
      @elems = [Elem.new(source, block)]
    end

    def or &block
      @elems << Elem.new(@source, block)

      self
    end
    def value
      @value ||= begin
        for elem in @elems
          result = elem.value
          return result if result != Par.bottom
        end

        Par.bottom
      end
    end
  end
end