module Par
  class Bottom
    def >> other
      self
    end
    def maybe
      Par.top
    end
    def bottom?
      true
    end
    def as obj=nil, &block
      self
    end

    def inspect
      'BOT'
    end
  end
end