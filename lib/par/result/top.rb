require 'par/result/conversion'

module Par
  class Top
    include Conversion

    def >> other
      other
    end
    def maybe
      self
    end
    def bottom?
      false
    end
  end
end
