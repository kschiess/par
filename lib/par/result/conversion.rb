module Par
  module Conversion
    def as obj=nil, &block
      obj = block.call(@obj || self) if block_given?

      return obj if obj == Par.top || obj == Par.bottom
      return obj if obj.kind_of? Result
      return Result.new(obj)
    end
  end # module Conversion
end # module Par